#!/bin/bash

DEVOPS_CI_SAAS_DIRECTORY="${BASH_SOURCE%/*}"
if [[ ! -d "$DEVOPS_CI_SAAS_DIRECTORY" ]]; then DEVOPS_CI_SAAS_DIRECTORY="$PWD"; fi

. "$DEVOPS_CI_SAAS_DIRECTORY/bitbucket.sh"
. "$DEVOPS_CI_SAAS_DIRECTORY/bluemix.sh"
. "$DEVOPS_CI_SAAS_DIRECTORY/confluence.sh"
. "$DEVOPS_CI_SAAS_DIRECTORY/hipchat.sh"
. "$DEVOPS_CI_SAAS_DIRECTORY/jfrog.sh"
. "$DEVOPS_CI_SAAS_DIRECTORY/jira.sh"
. "$DEVOPS_CI_SAAS_DIRECTORY/sonar.sh"