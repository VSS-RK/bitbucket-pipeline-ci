#!/bin/bash

DEVOPS_CI_LIB_CF_INSTALL()
{
	curl -L "https://cli.run.pivotal.io/stable?release=linux64-binary&source=github" | tar -zx
	mv cf /usr/local/bin
}

DEVOPS_CI_LIB_CF_VERSION()
{
	cf --version
}