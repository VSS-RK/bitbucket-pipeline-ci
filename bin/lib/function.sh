#!/bin/bash

DEVOPS_CI_FUNCTION_EXISTS()
{
	declare -f -F $1 > /dev/null
	return $?
}
