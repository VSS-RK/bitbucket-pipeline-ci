#!/bin/bash

export NODE_ENV=$DEVOPS_CI_NODE_END

DEVOPS_CI_LIB_NODE_INSTALL()
{
	export NVM_DIR=$DEVOPS_CI_NVM_DIR
	echo $DEVOPS_CI_NVM_URL
	exit
	curl $DEVOPS_CI_NVM_URL | bash
	. $NVM_DIR/nvm.sh

	nvm install $DEVOPS_CI_NODE_VERSION
	nvm alias default $DEVOPS_CI_NODE_END
	nvm use default
}

DEVOPS_CI_LIB_NODE_VERSION()
{
	npm -v
}