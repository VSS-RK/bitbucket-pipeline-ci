#!/bin/bash

DEVOPS_CI_CR()
{
	DEVOPS_CI_CR_FOLDER=$1
	for f in $(find $DEVOPS_CI_CR_FOLDER -name '*.sh'); do sed $'s/\r//' -i $f; done
}
