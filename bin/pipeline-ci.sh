#!/bin/bash

DEVOPS_CI_TIMESTAMP=$(date +%s)

DEVOPS_CI_WORKDIRECTORY=$(pwd)
DEVOPS_CI_BINDIRECTORY="${BASH_SOURCE%/*}"
DEVOPS_CI_DIRECTORY=$(echo ${DEVOPS_CI_BINDIRECTORY%/*})
DEVOPS_CI_LOGDIRECTORY="$DEVOPS_CI_WORKDIRECTORY/tmp"
DEVOPS_CI_LOGBUILDDIRECTORY="$DEVOPS_CI_LOGDIRECTORY/$DEVOPS_CI_TIMESTAMP"

if [[ ! -d "$DEVOPS_CI_BINDIRECTORY" ]]; then
	DEVOPS_CI_BINDIRECTORY="$PWD";
fi

if [[ ! -e $DEVOPS_CI_LOGBUILDDIRECTORY ]];then
	mkdir -p $DEVOPS_CI_LOGBUILDDIRECTORY
fi

. "$DEVOPS_CI_BINDIRECTORY/lib/lib.sh"
. "$DEVOPS_CI_BINDIRECTORY/build/build.sh"
. "$DEVOPS_CI_BINDIRECTORY/build/logs.sh"
. "$DEVOPS_CI_BINDIRECTORY/build/profile.sh"

if [[ -e "$DEVOPS_CI_WORKDIRECTORY/bin/build.sh" ]];then
	. "$DEVOPS_CI_WORKDIRECTORY/bin/build.sh"
fi

DEVOPS_CI_BUILD_PROFILE=$2

if [[ -z "$DEVOPS_CI_BUILD_PROFILE" ]]; then
	DEVOPS_CI_BUILD_PROFILE=$(DEVOPS_CI_BUILD_PROFILE_DIFFERENTIATE)
fi

DEVOPS_CI_BUILD_PROFILE=${DEVOPS_CI_BUILD_PROFILE,,}

case "$1" in

	build)
		DEVOPS_CI_BUILD $DEVOPS_CI_BUILD_PROFILE
		;;

	clean)
		DEVOPS_CI_BUILD_CLEAN $DEVOPS_CI_BUILD_PROFILE
		;;

	process-resources)
		DEVOPS_CI_BUILD_PROCESS_RESOURCES $DEVOPS_CI_BUILD_PROFILE
		;;
	
	validate)
		DEVOPS_CI_BUILD_VALIDATE $DEVOPS_CI_BUILD_PROFILE
		;;

	compile)
		DEVOPS_CI_BUILD_COMPILE $DEVOPS_CI_BUILD_PROFILE
		;;

	test)
		DEVOPS_CI_BUILD_TEST $DEVOPS_CI_BUILD_PROFILE
		;;
	
	scm)
		DEVOPS_CI_BUILD_SCM $DEVOPS_CI_BUILD_PROFILE
		;;

	package)
		DEVOPS_CI_BUILD_PACKAGE $DEVOPS_CI_BUILD_PROFILE
		;;

	install)
		DEVOPS_CI_BUILD_INSTALL $DEVOPS_CI_BUILD_PROFILE
		;;

	deploy)
		DEVOPS_CI_BUILD_DEPLOY $DEVOPS_CI_BUILD_PROFILE
		;;

	config)
		DEVOPS_CI_BUILD_CONFIG $DEVOPS_CI_BUILD_PROFILE
		;;

	report)
		DEVOPS_CI_BUILD_REPORT $DEVOPS_CI_BUILD_PROFILE
		;;

	*)
		printf "CEMEX $DEVOPS_CI_MESSAGE_FORMAT_STATUS_GREEN usage: $DEVOPS_CI_MESSAGE_FORMAT_STATUS_ORANGE [$DEVOPS_CI_MESSAGE_FORMAT_STATUS_ORANGE] [$DEVOPS_CI_MESSAGE_FORMAT_STATUS_ORANGE]\n" "Dev Ops CLI" "pipeline-ci.sh" "build, clean, config, process-resources, validate, compile, test, scm, package, install, deploy, doc" "development, feature, hotfix, release"
		;;

esac

if [[ ! -z "$1" && "$1" != "clean" ]]; then
	DEVOPS_CI_BUILD_LOGS
fi