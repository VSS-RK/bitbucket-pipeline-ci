#!/bin/bash

. "${BASH_SOURCE%/*}/config.sh"

DEVOPS_CI_BUILD_DEPLOY_REPOSITORY="dcmcp-npm-development"
DEVOPS_CI_BUILD_SCM_VERSION_MESSAGE="Devops CI feature pipeline build %s."
