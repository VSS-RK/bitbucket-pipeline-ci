#!/bin/bash

DEVOPS_CI_BUILD_VALIDATE_PROJECT()
{
	return 0
}
		
DEVOPS_CI_BUILD_COMPILE_PROJECT()
{
	return 0
}

DEVOPS_CI_BUILD_TEST_PROJECT()
{
	return 0
}

DEVOPS_CI_BUILD_PACKAGE_PROJECT()
{
	return 0
}

DEVOPS_CI_BUILD_INSTALL_PROJECT()
{
	return 0
}

DEVOPS_CI_BUILD_DEPLOY_PROJECT()
{
	return 0
}

DEVOPS_CI_BUILD_REPORT_PROJECT()
{
	return 0
}

DEVOPS_CI_BUILD_ISSUE_MANAGEMENT_PROJECT()
{
	return 0
}